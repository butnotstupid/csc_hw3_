﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using CasinoAnalysis;


namespace CasinoModel
{
    public interface IRepository
    {
        IQueryable<User> Users { get; }

        IQueryable<Bet> Bets { get; }

        IQueryable<GameResults> GameResults { get; }

        IQueryable<Deposite> Deposites { get; }

        bool AddUser(User user);

        bool UpdateUser(User user);

        bool DeleteUser(string username);

        bool AddBet(Bet bet);

        bool DeleteBet(int betId);

        bool AddDeposite(Deposite deposite);

        bool DeleteDeposite(int depositeId);

        bool AddGameResult(GameResults gameResult);

        bool DeleteGameResult(int gameResultId);
    }
}
