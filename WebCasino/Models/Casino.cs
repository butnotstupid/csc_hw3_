﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Instrumentation;
using System.Text;
using System.Threading.Tasks;
using WebCasino.Models;

namespace CasinoModel
{
    public class Casino
    {
        public const string PlayersLog = @"PlayersLog.txt";
        public const string RouletteLog = @"RouletteLog.txt";
        public const string DiceLog = @"DiceLog.txt";
        public const string BlackJackLog = @"BlackJackLog.txt";
        public const string MainLog = @"MainLog.txt";

        private const int MistakesAllowed = 5;

        public Dictionary<String, Int32> PlayerList;

        public void CreateLogFiles()
        {
            string path = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + PlayersLog;
            if (!File.Exists(path))
            {
                File.Create(path).Close();
            }
            path = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + RouletteLog;
            if (!File.Exists(path))
            {
                File.Create(path).Close();
            }
            path = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + DiceLog;
            if (!File.Exists(path))
            {
                File.Create(path).Close();
            }
            path = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + BlackJackLog;
            if (!File.Exists(path))
            {
                File.Create(path).Close();
            }
        }

        public Casino()
        {
            PlayerList = new Dictionary<string, int>();
            CreateLogFiles();
            LoadLog();
        }

        #region{ExpectStrings}

        public delegate bool Expected(String answer, int par = 0);

        public static bool ExpectName(String str, int par = 0)
        {
            return !String.IsNullOrEmpty(str) && str.All(Char.IsLetter);
        }
        public static bool ExpectYesNo(String str, int par = 0)
        {
            return !String.IsNullOrEmpty(str) && (str.Equals("y") || str.Equals("n"));
        }
        public static bool ExpectPositive(String str, int par = 0)
        {
            return !String.IsNullOrEmpty(str) && str.All(Char.IsDigit);
        }
        public static bool ExpectGameCode(String str, int par = 0)
        {
            return ExpectPositive(str) && Int32.Parse(str) <= 3 && Int32.Parse(str) >= 1;
        }
        public static bool ExpectBet(String str, int bound)
        {
            return ExpectPositive(str) && Int32.Parse(str) <= bound && Int32.Parse(str) > 0;
        }
        public static bool ExpectDice(String str, int par = 0)
        {
            return ExpectPositive(str) && Int32.Parse(str) <= 12 && Int32.Parse(str) >= 2;
        }
        public static bool ExpectRoulette(String str, int par = 0)
        {
            return (str.Equals("r") || str.Equals("b") || (ExpectPositive(str) && Int32.Parse(str) <= 36));
        }

        #endregion{123}

        private void LoadLog()
        {
            using (StreamReader streamReader = new StreamReader(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + PlayersLog))
            {
                while (!streamReader.EndOfStream)
                {
                    string str = streamReader.ReadLine();
                    if (str != null)
                    {
                        string[] nameBalance = str.Split(' ');
                        PlayerList.Add(nameBalance[0], Int32.Parse(nameBalance[1]));
                    }
                }
            }
        }

        private void RewriteLog()
        {
            string path = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + PlayersLog;
            
            using (StreamWriter streamWriter = new StreamWriter(path))
            {
                foreach (var element in PlayerList)
                {
                    streamWriter.WriteLine("{0} {1}", element.Key, element.Value);
                }
            }

        }

        public static void Log(string path, string message)
        {
            path = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + path;

            using (StreamWriter streamWriter = File.AppendText(path))
            {
                streamWriter.WriteLine(message);
            }
        }

        public static bool ReadAnswerDialog(String question, Expected expected, out String resultAnswer, int par = 0)
        {
            String read = null; int mistakesCount = MistakesAllowed;
            while (mistakesCount > 0)
            {
                Console.Write(question);
                read = Console.ReadLine();
                if (!expected(read, par))
                {
                    mistakesCount--;
                    Console.WriteLine("Incorrect input, please try again..");
                    if (mistakesCount == 1) Console.WriteLine("Attention! There's only one try remains!");
                }
                else break;
            }
            if (mistakesCount == 0)
            {
                Console.WriteLine("Sorry you exceeded mistakes limit. See you next time!");
                Console.ReadKey();
                resultAnswer = null;
                return false;
            }
            else
            {
                resultAnswer = read;
                return true;
            }
        }

        public static bool MakeBet(CasinoPlayer player, out int bet)
        {
            string betS; bet = 0;
            if (!Casino.ReadAnswerDialog("Enter your bet: ", Casino.ExpectBet, out betS, player.Balance)) return false;
            bet = Int32.Parse(betS);
            return true;
        }

        private int GameChange()  // returns game code (0 - incorrect change)
        {
            Console.WriteLine("Which game do you want to play?");
            Console.WriteLine("1. Black Jack");
            Console.WriteLine("2. Dice");
            Console.WriteLine("3. Roulette");

            string gamecode;
            if (!ReadAnswerDialog("Enter a gamecode (1-3): ", ExpectGameCode, out gamecode)) return 0;
            return Int32.Parse(gamecode);
        }

        private bool AddMoney(CasinoPlayer player)  // returns false if incorrect add
        {
            string readyn;
            if (!ReadAnswerDialog("Do you wish to add some money? (y/n): ", ExpectYesNo, out readyn)) return false;

            if (readyn.Equals("y"))
            {
                string add;
                if (!ReadAnswerDialog("Enter amount of money you want to add: ", ExpectPositive, out add)) return false;
                player.BalanceChange(Int32.Parse(add));
                Console.WriteLine("Added. Now your balance is {0}", player.Balance);
                Log(MainLog, "[" + DateTimeOffset.Now.ToString() + "] " + player.Name + " DEPOSITE:" + add);
            }
            return true;
        }

        public void Play()
        {
            Console.WriteLine("Welcome to CASINO CASINO!");

            string readname;
            if (!ReadAnswerDialog("Please let us know your name: ", ExpectName, out readname)) return;

            int balance;
            if (!PlayerList.ContainsKey(readname))
            {
                PlayerList.Add(readname, 0);
                balance = 0;
            }
            else balance = PlayerList[readname];
            CasinoPlayer player = new CasinoPlayer(readname, balance);
            
            Console.WriteLine("Nice to see you, {0}. Your current balance is: {1}", player.Name, player.Balance);

            if (!AddMoney(player)) return;

            int gc = GameChange();

            while (true && gc != 0)
            {
                bool gameError = false;
                switch (gc)
                {
                    case 1:
                        gameError = !BlackJack.Play(player);
                        break;
                     case 2:
                         gameError = !Dice.Play(player);
                         break;
                     case 3:
                         gameError = !Roulette.Play(player);
                         break;
                }

                if (gameError) return;

                Console.WriteLine("Good game, {0}. Your current balance is: {1}", player.Name, player.Balance);

                if (player.Balance == 0)
                {   
                    Console.WriteLine("Your money is over..");
                    if (!AddMoney(player)) return;
                    if (player.Balance == 0) return;
                }

                String playAgain;
                if (!ReadAnswerDialog("Play again? (y/n): ", ExpectYesNo, out playAgain)) return;
                if (playAgain.Equals("n"))
                {
                    Console.WriteLine("See you next time!");
                    break;
                }

                String sameGame;
                if (!ReadAnswerDialog("Same game? (y/n): ", ExpectYesNo, out sameGame)) return;
                if (sameGame.Equals("n")) gc = GameChange();
            }

            PlayerList[player.Name] = player.Balance;
            RewriteLog();   

            Console.ReadKey();
        }
    }
}
