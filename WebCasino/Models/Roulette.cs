﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using WebCasino.Models;

namespace CasinoModel
{
    class Roulette
    {
        public static bool Play(CasinoPlayer player)
        {
            Console.WriteLine("---ROULETTE GAME---");
            int bet;
            if (!Casino.MakeBet(player, out bet))
            {
                return false;
            }
            Casino.Log(Casino.MainLog, "[" + DateTimeOffset.Now.ToString() + "] " + player.Name + " " + CasinoAnalysis.GameCode.Roulet.ToString() + " BET:" + bet.ToString());

            Random random = new Random();
            int rouletteNumber = random.Next(0, 36);

            String guess;
            if (!Casino.ReadAnswerDialog("Guess color (r/b) or number (0-36): ", Casino.ExpectRoulette, out guess))
            {
                return false;
            }

            bool win;
            
            if ((guess.Equals("r") && rouletteNumber%2 == 1) || (guess.Equals("b") && rouletteNumber%2 == 0))
            {
                Console.WriteLine("You WON your bet: ");
                player.BalanceChange(+bet);
                win = true;
            }
            else if (Casino.ExpectPositive(guess) && Int32.Parse(guess) == rouletteNumber)
            {
                Console.WriteLine("You WON your bet: ");
                player.BalanceChange(+3*bet);
                win = true;
            }
            else
            {
                Console.WriteLine("You LOOSE your bet: ");
                player.BalanceChange(-bet);
                win = false;
            }

            Casino.Log(Casino.MainLog,
                "[" + DateTimeOffset.Now.ToString() + "] " + player.Name + " " + CasinoAnalysis.GameCode.Roulet.ToString() +
                " " + ((win) ? (CasinoAnalysis.GameResultStatus.Win.ToString() + " +") : (CasinoAnalysis.GameResultStatus.Loose.ToString()) + " -") +
                bet.ToString() + " " + guess + " " + rouletteNumber.ToString());

            Casino.Log(Casino.RouletteLog, 
                string.Format("Player: {0} | bet: {1} | guess: {2} | number: {3} | {4}", player.Name, bet, guess, rouletteNumber, (win) ? "WIN" : "LOOSE"));

            return true;
        }
    }
}
