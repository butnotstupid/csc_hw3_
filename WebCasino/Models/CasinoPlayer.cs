﻿using System;

namespace WebCasino.Models
{
    public class CasinoPlayer
    {
        public String Name { set; get; }
        public Int32 Balance { set; get; }
        public Int32 CurrentBet { set; get; }
        public Boolean LastGameWin { set; get; }

        public CasinoPlayer()
        {
        }

        public CasinoPlayer(String name, int balance)
        {
            Name = name;
            Balance = balance;
        }

        public void BalanceChange(int add)
        {
            Balance += add;
        }

    }

    public class DicePlayer : CasinoPlayer
    {
        public Int32 Sum { set; get; }

        public DicePlayer()
        {
            
        }

        public DicePlayer(String name, int balance)
        {
            Name = name;
            Balance = balance;
        }
    }

    public class RoulettePlayer : CasinoPlayer
    {
        public Int32 Number { set; get; }

        public RedBlack Color { set; get; }

        public String Input { set; get; }

        public RoulettePlayer()
        {

        }

        public RoulettePlayer(String name, int balance)
        {
            Name = name;
            Balance = balance;
        }
    }

    public enum RedBlack
    {
        Red, Black
    }
}
