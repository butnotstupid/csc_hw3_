﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CasinoAnalysis;
using Ninject;

namespace CasinoModel
{
    public class CasinoDBContext : DbContext
    {  
        public DbSet<User> Users { get; set; }

        public DbSet<Bet> Bets { get; set; }

        public DbSet<GameResults> GameResults { get; set; }

        public DbSet<Deposite> Deposites { get; set; }
    }
}