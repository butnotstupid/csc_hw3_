﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CasinoAnalysis;
using CasinoModel;
using Ninject;
using WebCasino.Models;

namespace WebCasino.Controllers
{
    public class HomeController : BaseController
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            //var users = Repository.Users.ToList();
            var user = new CasinoPlayer();

            return View(user);
        }

    }
}
