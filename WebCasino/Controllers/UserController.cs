﻿using System;
using System.Linq;
using System.Web.Mvc;
using CasinoAnalysis;
using CasinoModel;
using WebCasino.Models;

namespace WebCasino.Controllers
{
    public class UserController : BaseController
    {
        public ActionResult Login(User user)
        {
            if (!string.IsNullOrWhiteSpace(user.Name))
            {
                Repository.AddUser(user);
                user = Repository.Users.First(u => u.Name == user.Name);
                return View(user);
            }
            else
            {
                return View((User)null);
            }

        }

        public ActionResult AddMoney(string username, int balance)
        {
            var deposite = new Deposite();
            deposite.User = new User{Name = username, Balabce = balance};
            return View(deposite);
        }

        public ActionResult ChargeMoney(Deposite deposite)
        {
            var user = Repository.Users.FirstOrDefault(u => u.Name == deposite.User.Name);
            //var user = deposite.User;
            if (deposite.Value >= 0)
            {
                /*deposite.Date = DateTime.Now;
                Repository.AddDeposite(deposite);*/
                user.Balabce += deposite.Value;
                Repository.UpdateUser(user);
                return View("Login", user);
            }
            else
            {
                return View("Login", user);;
            }
        }


        public ActionResult PlayDice(string username)
        {
            var user = Repository.Users.FirstOrDefault(u => u.Name == username);
            DicePlayer player = new DicePlayer(username, user.Balabce);
            return View(player);
        }

        public ActionResult PartyDice(DicePlayer player)
        {
            Random random = new Random();

            int dice1 = random.Next(1, 6), dice2 = random.Next(1, 6);
            player.LastGameWin = dice1 + dice2 == player.Sum;
            player.BalanceChange(((player.LastGameWin) ? +1 : -1) * player.CurrentBet);

            var user = Repository.Users.FirstOrDefault(u => u.Name == player.Name);
            user.Balabce = player.Balance;
            Repository.UpdateUser(user);

            return View(player);
        }

        public ActionResult PlayRoulette(string username, int balance)
        {
            var user = Repository.Users.FirstOrDefault(u => u.Name == username);
            RoulettePlayer player = new RoulettePlayer(username, user.Balabce);
            return View(player);
        }

        public ActionResult PartyRoulette(RoulettePlayer player)
        {
            Random random = new Random();
            int rouletNumber = random.Next();

            if (player.Input == "red" || player.Input == "black")
            {
                RedBlack redBlack;
                RedBlack.TryParse(player.Input, out redBlack);
                player.Color = redBlack;
                if (rouletNumber%2 == 1 && player.Color == RedBlack.Red ||
                    rouletNumber%2 == 0 && player.Color == RedBlack.Black)
                {
                    player.LastGameWin = true;
                    player.BalanceChange(+player.CurrentBet);
                }
                else
                {
                    player.LastGameWin = false;
                    player.BalanceChange(-player.CurrentBet);
                }
            }
            else
            {
                player.Number = Int32.Parse(player.Input);
                if (rouletNumber == player.Number)
                {
                    player.LastGameWin = true;
                    player.BalanceChange(+3*player.CurrentBet);
                }
                else
                {
                    player.LastGameWin = false;
                    player.BalanceChange(-player.CurrentBet);
                }
            }

            var user = Repository.Users.FirstOrDefault(u => u.Name == player.Name);
            user.Balabce = player.Balance;
            Repository.UpdateUser(user);

            return View(player);
        }
    }
}